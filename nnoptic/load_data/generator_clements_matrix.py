import numpy as np


def clements_element(n, i, j, u, theta=None):
    if theta is None:
        theta = (np.pi / 4) + 0.5 * np.random.randn()
    u[i:j + 1, i:j + 1] = np.array([[np.cos(theta), np.sin(theta)],
                                    [np.sin(theta), -np.cos(theta)]], dtype=complex)
    return u


def save_base_clements_matrices(n, file_name):
    list_u = []
    for i in range(0, 5, 1):
        if (i % 2) == 0:
            u = np.eye(5, dtype=complex)
            u = clements_element(n, 0, 1, u)
            u = clements_element(n, 2, 3, u)
            list_u.append(u)
            u = clements_element(n, 0, 1, u)
            u = clements_element(n, 2, 3, u)
            list_u.append(u)
        else:
            u = np.eye(5, dtype=complex)
            u = clements_element(n, 1, 2, u)
            u = clements_element(n, 3, 4, u)
            list_u.append(u)
            u = clements_element(n, 1, 2, u)
            u = clements_element(n, 3, 4, u)
            list_u.append(u)

    file = open(file_name, 'w')
    for l in range(len(list_u)):
        for i in range(n):
            for j in range(n):
                file.write(str(list_u[l][i][j].real) + '\n')
                file.write(str(list_u[l][i][j].imag) + '\n')
        file.write('\n')
    file.close()
    print('Basis matrices are successfully generated and loaded to file')


def save_base_ideal_clements_matrices(n, file_name):
    list_u = []
    theta = np.pi / 4
    for i in range(0, 5, 1):
        if (i % 2) == 0:
            u = np.eye(5, dtype=complex)
            u = clements_element(n, 0, 1, u, theta=theta)
            u = clements_element(n, 2, 3, u, theta=theta)
            list_u.append(u)
            u = clements_element(n, 0, 1, u, theta=theta)
            u = clements_element(n, 2, 3, u, theta=theta)
            list_u.append(u)
        else:
            u = np.eye(5, dtype=complex)
            u = clements_element(n, 1, 2, u, theta=theta)
            u = clements_element(n, 3, 4, u, theta=theta)
            list_u.append(u)
            u = clements_element(n, 1, 2, u, theta=theta)
            u = clements_element(n, 3, 4, u, theta=theta)
            list_u.append(u)

    file = open(file_name, 'w')
    for l in range(len(list_u)):
        for i in range(n):
            for j in range(n):
                file.write(str(list_u[l][i][j].real) + '\n')
                file.write(str(list_u[l][i][j].imag) + '\n')
        file.write('\n')
    file.close()
    print('Basis matrices are successfully generated and loaded to file')


def get_random_phase_clements(n, _l):
    f = np.zeros((_l + 1, n))
    for i in range(0, _l, 1):
        if (i == 0) or (i == 1) or (i == 4) or (i == 5) or (i == 8) or (i == 9):
            f[i, 0], f[i, 2] = 2 * np.pi * np.random.rand(), 2 * np.pi * np.random.rand()
        else:
            f[i, 1], f[i, 3] = 2 * np.pi * np.random.rand(), 2 * np.pi * np.random.rand()
    f[-1] = 2 * np.pi * np.random.rand(n)
    return f
