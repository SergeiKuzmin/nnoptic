import numpy as np
from nnoptic.funcs_for_matrices import interferometer, create_list_fl

# This module creates a sample of size M from unitary matrices of size N by N, using as base matrices
# matrices from the file 'goal_matrices.txt' and saves this selection to the file 'sample_of_unitaries_matrices'


def save_sample_clements_matrices(n, _l, m, file_name1, file_name2):
    # Generate random phases for the sample (M different matrices, size N by N)
    fm = []
    for k in range(m):
        f = np.zeros((_l + 1, n))
        for i in range(0, _l, 1):
            if (i == 0) or (i == 1) or (i == 4) or (i == 5) or (i == 8) or (i == 9):
                f[i, 0], f[i, 2] = 2 * np.pi * np.random.rand(), 2 * np.pi * np.random.rand()
            else:
                f[i, 1], f[i, 3] = 2 * np.pi * np.random.rand(), 2 * np.pi * np.random.rand()
        f[-1] = 2 * np.pi * np.random.rand(n)
        fm.append(f)

    print(fm[0])

    # Create a list from U1, ..., UL
    file1 = open(file_name1, 'r')
    list_u = []
    for l in range(_l):
        u = np.zeros((n, n), dtype=complex)
        for i in range(n):
            for j in range(n):
                real = float(file1.readline())
                imag = float(file1.readline())
                u[i][j] = real + 1j * imag
        s = file1.readline()  # Read the empty line
        list_u.append(u)
    file1.close()

    print(list_u[0])

    um = []  # List of resulting unitary matrices of size N by N

    # We pass through the sample
    for k in range(m):
        um.append(interferometer(create_list_fl(fm[k]), list_u, n))

    file2 = open(file_name2, 'w')

    for k in range(m):  # We go through the selection and write phase matrices to a file
        for i in range(_l + 1):
            for j in range(n):
                file2.write(str(fm[k][i][j].real) + '\n')
        file2.write('\n')

    for k in range(m):  # We go through the selection and write the resulting selection of unitary matrices to a file
        for i in range(n):
            for j in range(n):
                file2.write(str(um[k][i][j].real) + '\n')
                file2.write(str(um[k][i][j].imag) + '\n')
        file2.write('\n')
    file2.close()
    print('Training dataset successfully loaded to file')
