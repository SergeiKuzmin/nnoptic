import numpy as np
from scipy import linalg

from nnoptic.funcs_for_matrices import generator_unitary_matrix, create_list_fl, interferometer, create_fourier_matrix


class Network(object):
    def __init__(self, number_modes, depth_scheme, size_train, mini_batch_size, file_name):
        # file_name - file to which the neural network is written
        # (trained basis matrices), and, accordingly, is read from it
        self.N = number_modes
        self.L = depth_scheme
        self.M = size_train
        self.mini_batch_size = mini_batch_size
        self.file_name = file_name

        # Initialization of a neural network
        # Initialize the full matrix basis with random unitary matrices
        self.list_U = np.array([generator_unitary_matrix(self.N) for _ in range(self.L)])

        # Initialize the full matrix basis with unitary Fourier matrices
        # self.list_U = np.array([create_fourier_matrix(self.N) for _ in range(self.L)])

        if file_name:  # If passed True, then the full matrix basis is initialized from file_name
            print('Load data about Network')
            file = open(file_name, 'r')
            for l in range(self.L):
                for i in range(self.N):
                    for j in range(self.N):
                        real = float(file.readline())
                        imag = float(file.readline())
                        self.list_U[l, i, j] = real + 1j * imag
                s = file.readline()  # Read the empty line
            file.close()

        # Initialized gradients for all elements of the full matrix basis
        self.grad_U = None
        self.grad_U_list = np.zeros((self.mini_batch_size, self.L, self.N, self.N), dtype=complex)
        self.list_A = np.zeros((self.mini_batch_size, self.L, self.N, self.N), dtype=complex)
        self.list_B = np.zeros((self.mini_batch_size, self.L, self.N, self.N), dtype=complex)

    def grad_frobenius(self, mini_batch_f, mini_batch_u):
        # This class method calculates the gradient self.grad_U

        # Calculate self.list_A and self.list_B
        for k in range(self.mini_batch_size):
            fl = create_list_fl(mini_batch_f[k])

            self.list_A[k][self.L - 1] = fl[self.L]
            for l in range(self.L - 2, -1, -1):
                self.list_A[k][l] = np.dot(self.list_A[k][l + 1], np.dot(self.list_U[l + 1], fl[l + 1]))
            self.list_B[k][0] = fl[0]
            for l in range(1, self.L, 1):
                self.list_B[k][l] = np.dot(np.dot(fl[l], self.list_U[l - 1]), self.list_B[k][l - 1])

            u_target = mini_batch_u[k]
            u_result = interferometer(fl, self.list_U, self.N)

            for l in range(self.L):
                for p in range(self.N):
                    for t in range(self.N):
                        a = self.list_A[k, l, :, :]
                        b = self.list_B[k, l, :, :]
                        grad_u_x = np.dot(a[:, p].reshape((self.N, 1)), b[t, :].reshape((1, self.N)))
                        grad_u_y = 1j * np.dot(a[:, p].reshape((self.N, 1)), b[t, :].reshape((1, self.N)))
                        grad_U_real = (2 / self.N) * np.sum((u_result - u_target).conj() * grad_u_x).real
                        grad_U_imag = (2 / self.N) * np.sum((u_result - u_target).conj() * grad_u_y).real
                        self.grad_U_list[k, l, p, t] = grad_U_real + 1j * grad_U_imag

        # Average the gradient over the mini-packet
        self.grad_U = np.sum(self.grad_U_list, axis=0) / self.mini_batch_size

    def sgd(self, rate_learning):
        self.list_U -= rate_learning * self.grad_U

    def polar_correct(self):
        # We replace the basis matrices with the closest unitary matrices
        # according to the Frobenius norm using polar decomposition
        for l in range(self.L):
            _u, _ = linalg.polar(self.list_U[l])
            self.list_U[l] = _u

    def save_network(self, file_name):
        file = open(file_name, 'w')
        for l in range(self.L):
            for i in range(self.N):
                for j in range(self.N):
                    file.write(str(self.list_U[l][i][j].real) + '\n')
                    file.write(str(self.list_U[l][i][j].imag) + '\n')
            file.write('\n')
        file.close()
