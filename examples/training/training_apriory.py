import numpy as np
import matplotlib.pyplot as plt
import time

from nnoptic import save_base_unitary_matrices, save_sample_unitary_matrices, get_random_phase
from nnoptic import trainer
from nnoptic.training import func_frobenius, derivative_func_frobenius
from nnoptic import frobenius_reduced
from nnoptic import Network

start_time = time.time()

N = 5  # Dimension and number of basis matrices
L = 10  # Number of layouts in linear optic scheme
M = 300  # Sample size of unitary matrices
mini_batch_size = 5  # Mini-packet size for one step of the Training algorithm
counts_of_epochs = 200  # Number of Training steps
coeff = 0.24  # Correctness coefficient of initialization of basis matrices
noisy_f = 0.00  # Parameter of noise for phase set
noisy_u = 0.00  # Parameter of noise for elements of matrices
method = 'L-BFGS-B'
# method = 'SGD'
label = r'$J_{FR}$'
func, grad_func, functional = func_frobenius, derivative_func_frobenius, frobenius_reduced

m = 1

file_name1 = 'goal_matrices.txt'
file_name2 = 'sample_of_unitaries_matrices.txt'
file_name3 = 'save_network.txt'

save_base_unitary_matrices(N, L, file_name1)
save_sample_unitary_matrices(N, L, M, file_name1, file_name2)

mean_results = np.zeros(counts_of_epochs)
mean_cross_validation = np.zeros(counts_of_epochs)

list_steps = []
list_results = []
list_cross_validation = []

epochs = []

for i in range(m):
    network = Network(N, L, M, mini_batch_size, False)
    steps, results, cross_validation, norma, error = trainer(file_name1, file_name2, False, N, L, M, mini_batch_size,
                                                             counts_of_epochs, func, grad_func, functional, coeff,
                                                             noisy_f, noisy_u, network, get_random_phase, method)
    list_steps.append(steps)
    list_results.append(results)
    list_cross_validation.append(cross_validation)

    mean_results += results
    mean_cross_validation += cross_validation

    epochs = steps

mean_results = mean_results / m
mean_cross_validation = mean_cross_validation / m

delta_time = time.time() - start_time
print('--- %s seconds ---' % delta_time)

fig, ax = plt.subplots()
plt.plot(epochs, mean_cross_validation, color='green', lw=2, label=label + ' Test set')
plt.plot(epochs, mean_results, color='blue', lw=2, label=label + ' Train set')
plt.tick_params(which='major', direction='in')
plt.tick_params(which='minor', direction='in')
plt.legend(loc="upper right")
ax.grid()
ax.minorticks_off()
plt.xlim(0, 200)
plt.yscale('log')
plt.xlabel('Epochs', fontsize=11)
plt.ylabel(label, fontsize=15)
plt.title('N = '+str(N)+', M = '+str(M)+', '+r'$\alpha$'+' ='+str(coeff))
plt.show()
